# Cuni.Arithmetics.FixedPoint Library Documentation

Library is implemented in C#. It has two main parts:

- ```Fixed<T>``` structure
- ```Q``` interface

## ```Fixed<T>``` Structure

This generic structure contains crucial operation method, such as:

- ```Add(Fixed<T> num)```: Method adds two fixed-point numbers and returns new instance of type Fixed\<T\>
- ```Substract(Fixed<T> subtrahend)```: Method subtracts two fixed-point numbers and returns new instance of type Fixed\<T\>.
- ```Multiply(Fixed<T> num)```: Method multiplies two fixed-point numbers and returns new instance of type Fixed\<T\>.
- ```Divide(Fixed<T> divisor)```: Method divides two fixed-point numbers and returns new instance of type Fixed\<T\>.

  Operator overloading (+,-,/,*) is implemented in the structure, so every operation can be called using proper operator:
  	```Fixed<T> f1 = f2 + f3;```, where f2,f3 are instances of class Fixed\<T\> **or** of type `int`. So syntax like this is permitted:
  	```Fixed<T> f1 = f2 + 3;```

There are two possibilities to initialize new instance:

- ```Fixed<T> f1 = new Fixed<T>(3);```
- ```Fixed<T> f2 = 3;```

Generic type **T** has to be descendant of interface **Q**.

## Q interface

Every type of fixed-point has to inherit from this interface. It has three components:

- ```byte NumberOfRealFixedPointBits { get;}``` property
- ```int Value{ get; set; }``` property:  fixed-point value is saved here (Possible improvement: move the property to Fixed\<T\> structure)
- ```void ComputeFixedPointFromInt(int num)``` method: Fixed point number is computed using shift operation

Currently there are three types of fixed-point structures inheriting this interface:

- ```struct Q8__24```: 8 bits for integer part, 24 bits for real part 
- ```struct Q16_16```: 16 bits for integer part, 16 bits for real part
- ```struct Q24_8```: 8 bits for integer part, 24 bits for real part

## Unit Tests

Solution contains unit test project, which are testing all possible scenarios.

## Benchmarks

Solution contains benchmark project, where two functions are implemented:

1. Gauss Elimination for type ```double```
2. Gauss Elimination for type ```Fixed<T>```

From benchmarking one can observe that ```double``` implementation is almost twice faster than ```Fixed<T>``` implementation.