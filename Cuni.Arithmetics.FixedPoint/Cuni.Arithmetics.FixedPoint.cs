﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("FixedPointTest")]
namespace Cuni.Arithmetics.FixedPoint
{
    /// <summary>
    /// Class representing fixed point notation with basic operations implemented
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public struct Fixed<T> :IEquatable<Fixed<T>>,IComparable<Fixed<T>> where T : Q, new()
    {
        public int CompareTo(Fixed<T> other)
        {
            return this.fixedPointNumber.Value.CompareTo(other.fixedPointNumber.Value);
        }
        public bool Equals(Fixed<T> other)
        {
            if (this.fixedPointNumber.Value == other.fixedPointNumber.Value)
                return true;
            else return false;
        }
        /// <summary>
        /// Represents fixed point number, has property .Value where the value of integer is stored
        /// </summary>
        public T fixedPointNumber;
        public Fixed<T> Add(Fixed<T> q1)
        {
            Fixed<T> result = new Fixed<T>();
            result.fixedPointNumber.Value = fixedPointNumber.Value + q1.fixedPointNumber.Value;
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="q1">Subtrahend</param>
        /// <returns></returns>
        public Fixed<T> Subtract(Fixed<T> q1)
        {
            Fixed<T> result = new Fixed<T>();
            result.fixedPointNumber.Value = fixedPointNumber.Value - q1.fixedPointNumber.Value;
            return result;
        }
        public Fixed<TTo> ToFixed<TTo>() where TTo:Q, new()
        {
            TTo s = new TTo();
            T t = new T();
            if (s.NumberOfRealFixedPointBits <= t.NumberOfRealFixedPointBits)
            {
                Fixed<TTo> result = new Fixed<TTo>();
                result.fixedPointNumber.Value = fixedPointNumber.Value >> (t.NumberOfRealFixedPointBits - s.NumberOfRealFixedPointBits);
                return result;
            }
            else
            {
                Fixed<TTo> result = new Fixed<TTo>();
                result.fixedPointNumber.Value= fixedPointNumber.Value << (s.NumberOfRealFixedPointBits - t.NumberOfRealFixedPointBits);
                return result;
            }
        }
        public static implicit operator Fixed<T>(int value)
        {
            return new Fixed<T>(value);
        }

        public static Fixed<T> operator +(Fixed<T> a, Fixed<T> b)
        {
            return a.Add(b);
        }
        public static Fixed<T> operator +(Fixed<T> a, int b)
        {
            Fixed<T> tmp = b;
            return a.Add(tmp);
        }
        public static Fixed<T> operator -(Fixed<T> a, Fixed<T> b)
        {
            return a.Subtract(b);
        }
        public static Fixed<T> operator -(Fixed<T> a, int b)
        {
            Fixed<T> tmp = b;
            return a.Subtract(tmp);
        }
        public static Fixed<T> operator /(Fixed<T> a, Fixed<T> b)
        {
            return a.Divide(b);
        }
        public static Fixed<T> operator /(Fixed<T> a, int b)
        {
            Fixed<T> tmp = b;
            return a.Divide(tmp);
        }
        public static Fixed<T> operator *(Fixed<T> a, Fixed<T> b)
        {
            return a.Multiply(b);
        }
        public static Fixed<T> operator *(Fixed<T> a, int b)
        {
            Fixed<T> tmp = b;
            return a.Multiply(tmp);
        }
        public Fixed<T> Multiply(Fixed<T> q1)
        {
            Fixed<T> result = new Fixed<T>();
            long tmp = 0;
            for (int i = 0; i<32;i++)
            {
                if(IsBitSet(q1.fixedPointNumber.Value, i))
                {
                    tmp += ((long)fixedPointNumber.Value << i);
                }
            }
            tmp = (tmp >> result.fixedPointNumber.NumberOfRealFixedPointBits);
            result.fixedPointNumber.Value = (int)tmp;
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="q1">Divisor</param>
        /// <returns></returns>
        public Fixed<T> Divide(Fixed<T> q1)
        {
            Fixed<T> result = new Fixed<T>();
            result.fixedPointNumber.Value=(int)((((long)fixedPointNumber.Value*(1<<result.fixedPointNumber.NumberOfRealFixedPointBits))/((long)q1.fixedPointNumber.Value)));
            return result;
        }
        public Fixed(int a)
        {
            fixedPointNumber = new T();
            fixedPointNumber.ComputeFixedPointFromInt(a);
        }
        /// <summary>
        /// Checks if particular bit of an integer is set
        /// </summary>
        /// <param name="b"></param>
        /// <param name="pos">Position of bit</param>
        /// <returns>True if set, false if not</returns>
        private bool IsBitSet(int b, int pos)
        {
            return (b & (1 << pos)) != 0;
        }
        /// <summary>
        /// Double value is computed using 'ComputeDoubleValueFromInt(int a)' function
        /// </summary>
        public double DoubleValueOfFixedPoint { get { return ComputeDoubleValueFromInt(); } }

        internal double ComputeDoubleValueFromInt()
        {
            double result = (double)(fixedPointNumber.Value >> fixedPointNumber.NumberOfRealFixedPointBits);
            for(int i = fixedPointNumber.NumberOfRealFixedPointBits-1; i>=0;i--)
            {
                if(IsBitSet(fixedPointNumber.Value, i))
                {
                    result += Math.Pow(2, -(fixedPointNumber.NumberOfRealFixedPointBits - i));
                }
            }
            return result;
        }
        public override string ToString()
        {
            return DoubleValueOfFixedPoint.ToString();
        }
    }
    /// <summary>
    /// interface representing all types of fixed point integers, which are implemented. Every fixed point struct inherits from this interface
    /// </summary>
    public interface Q
    {
        byte NumberOfRealFixedPointBits { get;}
        int Value{ get; set; }
        void ComputeFixedPointFromInt(int a);
    }
    
    /// <summary>
    /// Struct represents fixed point notation, where first eight bits are used for integer part and the rest for real part
    /// </summary>
    public struct Q8_24:Q
    {
        public byte NumberOfRealFixedPointBits
        {
            get
            {
                return 24;
            }
        }
        private int val;
        public int Value { get { return val; } set { val = value; } }
        
        public void ComputeFixedPointFromInt(int a)
        {
            val = a << 24;
        }
    }
    /// <summary>
    /// Struct represents fixed point notation, where first 16 bits are used for integer part and the rest for real part
    /// </summary>
    public struct Q16_16 : Q
    {
        public byte NumberOfRealFixedPointBits
        {
            get
            {
                return 16;
            }
        }
        private int val;
        public int Value { get { return val; } set { val = value; } }

        public void ComputeFixedPointFromInt(int a)
        {
            val = a << 16;
        }
    }
    /// <summary>
    /// Struct represents fixed point notation, where first eight bits are used for integer part and the rest for real part
    /// </summary>
    public struct Q24_8 : Q
    {
        public byte NumberOfRealFixedPointBits
        {
            get
            {
                return 8;
            }
        }
        private int val;
        public int Value { get { return val; } set { val = value; } }

        public void ComputeFixedPointFromInt(int a)
        {
            val = a << 8;
        }
    }
}
