﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cuni.Arithmetics.FixedPoint;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;


namespace FixedPointBenchmark
{
    public class Program
    {
        public class AddTests
        {
            double[,] matrix1 = { { 2, 1, -1 }, { -3, -1, 2 }, { -2, 1, 2 } };
            double[] b1 = { 8, -11, -3 };
            int n = 3;
            Fixed<Q16_16>[,] matrix3 = { { 2, 1, -1 }, { -3, -1, 2 }, { -2, 1, 2 } };
            Fixed<Q16_16>[] b3 = { 8, -11, -3 };
            [Benchmark]
            public double[] GaussDoubleTest()
            {
                return GaussDouble(matrix1,b1, n);
            }

            [Benchmark]
            public Fixed<Q16_16>[] GaussFixedTest()
            {
                return GaussFixed(matrix3, b3, n);
            }


        }
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<AddTests>();
            double[,] matrix = { { -1, 1.5 }, { 1, -1 } };
            double[] b = { 1, 1 };
            int n = 2;
            double[] x = GaussDouble(matrix, b, n);
            double[,] matrix1 = { { 2,1,-1}, {-3,-1,2 },{ -2,1,2 } };
            double[] b1 = { 8,-11,-3 };
            int n1 = 3;
            double[] x1 = GaussDouble(matrix1, b1, n1);


            Fixed<Q16_16>[,] matrix2 = { { -1, 1 }, { 1, 1 } };
            Fixed<Q16_16>[] b2 = { 1, 1 };
            int n2 = 2;
            Fixed<Q16_16>[] x2 = GaussFixed<Q16_16>(matrix2, b2, n2);
            Fixed<Q16_16>[,] matrix3 = { { 2, 1, -1 }, { -3, -1, 2 }, { -2, 1, 2 } };
            Fixed<Q16_16>[] b3 = { 8, -11, -3 };
            int n3 = 3;
            Fixed<Q16_16>[] x3 = GaussFixed(matrix3, b3, n3);
            Console.ReadKey();
            
        }
        public static double[] GaussDouble(double[,] matrix, double[] b, int n)
        {
            for (int k = 0; k < n-1; k++)
            {
                for (int i = k + 1; i < n; i++)
                {
                    if (i < n)
                    {
                        matrix[i, k] /= matrix[k, k];
                        for (int j = k + 1; j < n; j++)
                        {
                            matrix[i, j] -= (matrix[i, k] * matrix[k, j]);
                        }
                    }
                }
            }
            for (int k = 0; k < n-1 ; k++)
            {
                for (int i = k + 1; i < n; i++)
                {
                    b[i] -= matrix[i, k] * b[k];
                }
            }
            double[] x = new double[n];
            for (int i = n - 1; i >= 0; i--)
            {
                double s = b[i];
                for (int j = i+1; j < n; j++)
                {
                    if (j < n)
                    {
                        s -= matrix[i, j] * x[j];
                    }
                }
                x[i] = s / matrix[i, i];
            }
            return x;
        }
        public static Fixed<T>[] GaussFixed<T>(Fixed<T>[,] matrix, Fixed<T>[] b, int n) where T:struct,Q
        {
            for (int k = 0; k < n - 1; k++)
            {
                for (int i = k + 1; i < n; i++)
                {
                    if (i < n)
                    {
                        matrix[i, k] /= matrix[k, k];
                        for (int j = k + 1; j < n; j++)
                        {
                            matrix[i, j] -= (matrix[i, k] * matrix[k, j]);
                        }
                    }
                }
            }
            for (int k = 0; k < n - 1; k++)
            {
                for (int i = k + 1; i < n; i++)
                {
                    b[i] -= matrix[i, k] * b[k];
                }
            }
            Fixed<T>[] x = new Fixed<T>[n];
            for (int i = n - 1; i >= 0; i--)
            {
                Fixed<T> s = b[i];
                for (int j = i + 1; j < n; j++)
                {
                    if (j < n)
                    {
                        s -= matrix[i, j] * x[j];
                    }
                }
                x[i] = s / matrix[i, i];
            }
            return x;
        }
    }
}
