﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using X= Cuni.Arithmetics.FixedPoint;
namespace FixedPointTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CompareToSameAsTest()
        {
            X.Fixed<X.Q8_24> a1 = 3;
            X.Fixed<X.Q8_24> a2 = 3;
            Assert.AreEqual(0, a1.CompareTo(a2));
        }
        [TestMethod]
        public void CompareToLessThanTest()
        {
            X.Fixed<X.Q8_24> a1 = 3;
            X.Fixed<X.Q8_24> a2 = 2;
            Assert.AreEqual(1, a1.CompareTo(a2));
        }
        [TestMethod]
        public void CompareToBiggerThanTest()
        {
            X.Fixed<X.Q8_24> a1 = 2;
            X.Fixed<X.Q8_24> a2 = 3;
            Assert.AreEqual(-1, a1.CompareTo(a2));
        }
        [TestMethod]
        public void EqualsTest()
        {
            X.Fixed<X.Q8_24> a1 = 2;
            X.Fixed<X.Q8_24> a2 = 2;
            Assert.AreEqual(true, a1.Equals(a2));
        }
        [TestMethod]
        public void ConvertQ8_24ToQ16_16()
        {
            X.Fixed<X.Q8_24> a1 = 2;
            X.Fixed<X.Q16_16> a2 = a1.ToFixed<X.Q16_16>();
            Assert.AreEqual("2", a2.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void ConvertQ8_24ToQ24_8()
        {
            X.Fixed<X.Q8_24> a1 = 2;
            X.Fixed<X.Q24_8> a2 = a1.ToFixed<X.Q24_8>();
            Assert.AreEqual("2", a2.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void ConvertToQ8_24FromQ24_8()
        {
            X.Fixed<X.Q24_8> a2 = 2;
            X.Fixed<X.Q8_24> a1 = a2.ToFixed<X.Q8_24>();
            Assert.AreEqual("2", a2.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void ComputeFixedPointQ8_24NumberFromInt()
        {
            X.Q8_24 a = new X.Q8_24();
            a.ComputeFixedPointFromInt(2);
            Assert.AreEqual(33554432, a.Value);

        }
        [TestMethod]
        public void ComputeFixedPointQ16_16NumberFromInt()
        {
            X.Q16_16 a = new X.Q16_16();
            a.ComputeFixedPointFromInt(2);
            Assert.AreEqual(131072, a.Value);

        }
        [TestMethod]
        public void ComputeFixedPointQ24_8NumberFromInt()
        {
            X.Q24_8 a = new X.Q24_8();
            a.ComputeFixedPointFromInt(2);
            Assert.AreEqual(512, a.Value);

        }
        [TestMethod]
        public void AdditionFixedPointQ8_24BothPositive()
        {
            
            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(2);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(3);
            X.Fixed<X.Q8_24> result = a1.Add(a2);
            Assert.AreEqual(83886080,result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void PlusOperatorOverloadTestQ8_24()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(2);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(3);
            X.Fixed<X.Q8_24> result = a1+a2;
            Assert.AreEqual(83886080, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void PlusOperatorOverloadTestQ8_24PlusInt()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(2);
            X.Fixed<X.Q8_24> result = a1 + 3;
            Assert.AreEqual(83886080, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void PlusOperatorOverloadTestQ8_24PlusIntReversed()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(2);
            X.Fixed<X.Q8_24> result = 3+a1;
            Assert.AreEqual(83886080, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void TestEqualSignOverloadQ8_24()
        {

            X.Fixed<X.Q8_24> a1 = 2;
            X.Fixed<X.Q8_24> a2 = 3;
            X.Fixed<X.Q8_24> result = a1 + a2;
            Assert.AreEqual(83886080, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void TestEqualSignOverloadQ16_16()
        {

            X.Fixed<X.Q16_16> a1 = 2;
            X.Fixed<X.Q16_16> a2 = 3;
            X.Fixed<X.Q16_16> result = a1 + a2;
            Assert.AreEqual("5", result.ToString());

        }
        [TestMethod]
        public void TestEqualSignOverloadQ24_8()
        {

            X.Fixed<X.Q24_8> a1 = 2;
            X.Fixed<X.Q24_8> a2 = 3;
            X.Fixed<X.Q24_8> result = a1 + a2;
            Assert.AreEqual("5", result.ToString());

        }
        [TestMethod]
        public void AdditionFixedPointQ16_16BothPositive()
        {

            X.Fixed<X.Q16_16> a1 = new X.Fixed<X.Q16_16>(10);
            X.Fixed<X.Q16_16> a2 = new X.Fixed<X.Q16_16>(48);
            X.Fixed<X.Q16_16> result = a1.Add(a2);
            Assert.AreEqual("58", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void SubtractionFixedPointQ16_16()
        {

            X.Fixed<X.Q16_16> a1 = new X.Fixed<X.Q16_16>(10);
            X.Fixed<X.Q16_16> a2 = new X.Fixed<X.Q16_16>(48);
            X.Fixed<X.Q16_16> result = a1.Subtract(a2);
            Assert.AreEqual("-38", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void AdditionFixedPointQ16_16OnePositiveOneNegative()
        {

            X.Fixed<X.Q16_16> a1 = new X.Fixed<X.Q16_16>(-10);
            X.Fixed<X.Q16_16> a2 = new X.Fixed<X.Q16_16>(48);
            X.Fixed<X.Q16_16> result = a1.Add(a2);
            Assert.AreEqual("38", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void AdditionFixedPointQ24_8BothPositive()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(1048);
            X.Fixed<X.Q24_8> result = a1.Add(a2);
            Assert.AreEqual("1158", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void SubtractionFixedPointQ24_8()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(1048);
            X.Fixed<X.Q24_8> result = a1.Subtract(a2);
            Assert.AreEqual("-938", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void MinusOperatorOverloadQ24_8()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(1048);
            X.Fixed<X.Q24_8> result = a1-a2;
            Assert.AreEqual("-938", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void MinusOperatorOverloadQ24_8PlusInt()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> result = a1 - 1048;
            Assert.AreEqual("-938", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void MinusOperatorOverloadQ24_8PlusIntReversed()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> result = 1048-110;
            Assert.AreEqual("938", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void AdditionFixedPointQ24_8OnePositiveOneNegative()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(-1048);
            X.Fixed<X.Q24_8> result = a1.Add(a2);
            Assert.AreEqual("-938", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void AdditionFixedPointQ8_24OnePositiveOneNegative()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(-100);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(3);
            X.Fixed<X.Q8_24> result = a1.Add(a2);
            Assert.AreEqual(-1627389952, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void SubtractionFixedPointQ8_24OnePositiveOneNegative()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(-100);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(3);
            X.Fixed<X.Q8_24> result = a1.Subtract(a2);
            Assert.AreEqual("-103", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void MultiplyFixedPointQ8_24BothPositive()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(13);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(19);
            X.Fixed<X.Q8_24> result =a1.Multiply(a2);
            Assert.AreEqual(-150994944, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void MultiplyOperatorOverloadTestQ8_24()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(13);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(19);
            X.Fixed<X.Q8_24> result = a1*a2;
            Assert.AreEqual(-150994944, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void MultiplyOperatorOverloadTestQ8_24TimesInt()
        {

            
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(19);
            X.Fixed<X.Q8_24> result = 13 * a2;
            Assert.AreEqual(-150994944, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void MultiplyOperatorOverloadTestQ8_24TimesIntReversed()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(13);
            X.Fixed<X.Q8_24> result = a1 * 19;
            Assert.AreEqual(-150994944, result.fixedPointNumber.Value);

        }
        [TestMethod]
        public void DivideFixedPointQ8_24BothPositive()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(248);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(10);
            X.Fixed<X.Q8_24> result = a1.Divide(a2);
            Assert.AreEqual("-0,799999952316284", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void DivideOperatorOverloadTestQ8_24()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(248);
            X.Fixed<X.Q8_24> a2 = new X.Fixed<X.Q8_24>(10);
            X.Fixed<X.Q8_24> result = a1/a2;
            Assert.AreEqual("-0,799999952316284", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void DivideOperatorOverloadTestQ8_24DividedByInt()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(248);
            X.Fixed<X.Q8_24> result = a1 / 10;
            Assert.AreEqual("-0,799999952316284", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void DivideOperatorOverloadTestIntDividedByQ8_24()
        {

            X.Fixed<X.Q8_24> a1 = new X.Fixed<X.Q8_24>(10);
            X.Fixed<X.Q8_24> result = 248 / a1;
            Assert.AreEqual("-0,799999952316284", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void DivideFixedPointQ24_8()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(10);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(4);
            X.Fixed<X.Q24_8> result = a1.Divide(a2);
            Assert.AreEqual("2,5", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void MultiplyFixedPointQ24_8()
        {

            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(-110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(1048);
            X.Fixed<X.Q24_8> result = a1.Multiply(a2);
            Assert.AreEqual("-115280", result.DoubleValueOfFixedPoint.ToString());

        }
        [TestMethod]
        public void MultiplyFixedPointQ16_16()
        {

            X.Fixed<X.Q16_16> a1 = new X.Fixed<X.Q16_16>(-10);
            X.Fixed<X.Q16_16> a2 = new X.Fixed<X.Q16_16>(-48);
            X.Fixed<X.Q16_16> result = a1.Multiply(a2);
            Assert.AreEqual("480", result.DoubleValueOfFixedPoint.ToString());
        }
        [TestMethod]
        public void ComputeDoubleFromFixedPointQ24_8()
        {
            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(128);
            Assert.AreEqual("128", a1.ComputeDoubleValueFromInt().ToString());

        }
        [TestMethod]
        public void ComputeDoubleFromFixedPointQ16_16()
        {
            X.Fixed<X.Q16_16> a1 = new X.Fixed<X.Q16_16>(-110);
            Assert.AreEqual("-110", a1.ComputeDoubleValueFromInt().ToString());

        }
        [TestMethod]
        public void TestingImmutabilityAddition()
        {
            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(-1048);
            X.Fixed<X.Q24_8> result = a1.Add(a2);
            Assert.AreEqual("110", a1.DoubleValueOfFixedPoint.ToString());
            Assert.AreEqual("-1048", a2.DoubleValueOfFixedPoint.ToString());
        }
        [TestMethod]
        public void TestingImmutabilitySubtraction()
        {
            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(-1048);
            X.Fixed<X.Q24_8> result = a1.Subtract(a2);
            Assert.AreEqual("110", a1.DoubleValueOfFixedPoint.ToString());
            Assert.AreEqual("-1048", a2.DoubleValueOfFixedPoint.ToString());
        }
        [TestMethod]
        public void TestingImmutabilityMultiplication()
        {
            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(-1048);
            X.Fixed<X.Q24_8> result = a1.Multiply(a2);
            Assert.AreEqual("110", a1.DoubleValueOfFixedPoint.ToString());
            Assert.AreEqual("-1048", a2.DoubleValueOfFixedPoint.ToString());
        }
        [TestMethod]
        public void TestingImmutabilityDivision()
        {
            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            X.Fixed<X.Q24_8> a2 = new X.Fixed<X.Q24_8>(-1048);
            X.Fixed<X.Q24_8> result = a1.Divide(a2);
            Assert.AreEqual("110", a1.DoubleValueOfFixedPoint.ToString());
            Assert.AreEqual("-1048", a2.DoubleValueOfFixedPoint.ToString());
        }
        [TestMethod]
        public void TestToStringMethod()
        {
            X.Fixed<X.Q24_8> a1 = new X.Fixed<X.Q24_8>(110);
            Assert.AreEqual("110", a1.DoubleValueOfFixedPoint.ToString());
        }
    }

}
